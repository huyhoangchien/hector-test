# GITLAB CI/CD

## Tech stack
* I use gitlab to store the source code and use its ci/cd 
* AWS beanstalk to run the application in AWS
* AWS ECR to store the docker image

## How to trigger the build
* Simply commit on master branch, the flow is:
Commit on master branch -> CI/CD will build the image -> push image to ECR -> update AWS beanstalk application
* Read the .gitlab-ci.yml for more details

## Something you should notice
* Instead of release or staging branch, I just use master branch for this (the concept should be the same)
* The content should be `Hello World! 19825b4b - production` (19825b4b is the git commit SHA)
* To access the webapp, use this link http://hectortest-env.eba-cgmzk4qb.us-west-1.elasticbeanstalk.com/
* I used to work with jenkins, but the test is so simple so that I decide to use gitlab - as a chance to work with its ci/cd
* Please request me if you want to edit the file
* The pipline progress is here: https://gitlab.com/huyhoangchien/hector-test/-/pipelines